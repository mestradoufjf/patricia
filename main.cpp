#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <float.h>

#include "InvertedIndex.h"

#define MAIN_MENU 1
#define SEARCH_MENU 2
#define PRINT_MENU 3

#define INIT_OPTION 1
#define SEARCH_OPTION 2
#define PRINT_OPTION 3

#define FILE_OPTION 1
#define STDOUT_OPTION 2

void clear_screen()
{
#ifdef WINDOWS
    system("cls");
#else
    system("clear");
#endif
}

int test_suite(char* input)
{
	FILE *input_file;
	unsigned int file_count = 0, word_count = 0;
	char **file_names;
	RadixTree* tree = NULL;

	time_t start, end;
	double time, min_time = FLT_MAX, max_time = FLT_MIN, avg_time = 0.0;

	input_file = fopen(input, "r");

	if(input_file){
		file_names = getFileNames(input_file, &file_count);
		fclose(input_file);
	}
	else{
		fprintf(stderr, "Fatal error: cannot read input file!\n");
		exit(1);
	}

	fprintf(stdout, "Test suite %s:\n", input);
	tree = createInvertedIndex(file_names, file_count);

	if(tree){
		start = clock();
		tree->printToFile(stdout);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		fprintf(stdout, "Print time:\n");
		fprintf(stdout, "time: %6.10lf\n", time);

		fprintf(stdout, "Search times:\n");
		/// Palavras ausentes
		start = clock();
		searchDocumentRelevance("englush", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("hipopotomonstrossesquipedaliofobico", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("jaiminho", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("Pneumoultramicroscopicossilicovulcanoconiose", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("arvorepatricia", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		/// Palavras presentes
		start = clock();
		searchDocumentRelevance("Gutenberg", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("several", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("to", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("newspaper", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("asterisk", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		/// Frases
		start = clock();
		searchDocumentRelevance("To create these etexts", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("quem com ferro fere com ferro sera ferido", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("THIS ETEXT IS OTHERWISE painfully elaborate their projects", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("Nevertheless the organization of most of the assemblies takes place", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		start = clock();
		searchDocumentRelevance("With still greater boldness Montpellier enjoined all representatives", tree, file_count, 0.0);
		end = clock();
		time = (double)(end - start) / CLOCKS_PER_SEC;
		if(time < min_time) min_time = time;
		if(time > max_time) max_time = time;
		avg_time += time;
		word_count++;

		avg_time = avg_time/(double)(word_count);
		fprintf(stdout, "min: %6.10lf max: %6.10lf avg: %6.10lf\n", min_time, max_time, avg_time);

		delete tree;
	}

	return 0;
}

int menu(int submenu){
	int option = 9;
	char line[32];

	switch(submenu){
		case MAIN_MENU:
			do{
				clear_screen();
				printf("*******************************************************\n");
				printf("                    Inverted Index\n");
				printf("*******************************************************\n");
				printf("Choose one of the following options:\n");
				printf("1 - Create inverted index\n");
				printf("2 - Search word\n");
				printf("3 - Print inverted index\n");
				printf("0 - Exit\n");
				fgets(line, 32, stdin);
				sscanf(line, "%d", &option);
			}while(option < 0 || option > 3);
			break;
		case SEARCH_MENU:
			clear_screen();
			printf("*******************************************************\n");
			printf("                    Inverted Index\n");
			printf("*******************************************************\n");
			printf("Type the search words:\n");
			break;
		case PRINT_MENU:
			do{
				clear_screen();
				printf("*******************************************************\n");
				printf("                    Inverted Index\n");
				printf("*******************************************************\n");
				printf("Choose output:\n");
				printf("1 - File (inverted_index.out)\n");
				printf("2 - Standard output\n");
				printf("0 - Back\n");
				fgets(line, 32, stdin);
				sscanf(line, "%d", &option);
			}while(option < 0 || option > 2);
			break;
	}
	return option;
}

int main(int argc, char* argv[]){
	FILE *input_file = NULL, *output_file = NULL;
	int option = 0, option2 = 0;
	unsigned int file_count = 0;
	float L = 1.0;
	char search[1024], line[32];
	char **file_names = NULL;
	RadixTree* tree = NULL;

	if(argc < 2){
		fprintf(stderr, "Error: input file not provided.\nUsage: ./InvertedIndex <input_file>\n");
		return EXIT_FAILURE;
	}

	if(TEST)
		return test_suite(argv[1]);
	else{
		do{
		option = menu(MAIN_MENU);
			switch(option){
				case INIT_OPTION:
					input_file = fopen(argv[1], "r");

					if(input_file){
						file_names = getFileNames(input_file, &file_count);
						fclose(input_file);
					}
					else{
						fprintf(stderr, "Fatal error: cannot read input file!\n");
						exit(1);
					}

					tree = createInvertedIndex(file_names, file_count);

					if(tree) printf("Inverted index created successfully.\n");
					getchar();

					break;
				case SEARCH_OPTION:
					if(tree){
						option2 = menu(SEARCH_MENU);
						fgets(search, 1024, stdin);
						printf("Type the minimum relevance for documents found [0,1]:\n");
						fgets(line, 32, stdin);
						sscanf(line, "%f", &L);
						searchDocumentRelevance(search, tree, file_count, L);
						getchar();
					}
					else{
						printf("Error: Inverted index not initialized. Choose option 1 before attempting this operation.\n");
						getchar();
					}
					break;
				case PRINT_OPTION:
					if(tree){
						option2 = menu(PRINT_MENU);
						if(option2 == FILE_OPTION){
							output_file = fopen("inverted_index.out", "w");
							tree->printToFile(output_file);
							fclose(output_file);
							printf("Inverted index printed successfully into 'inverted_index.out'.\n");
						}
						else{
							if(option2 == STDOUT_OPTION){
								tree->printToFile(stdout);
								printf("Inverted index printed successfully.\n");
							}
						}
						getchar();
					}
					else{
						printf("Error: Inverted index not initialized. Choose option 1 before attempting this operation.\n");
						getchar();
					}
					break;
			}
		}while(option);
	}

	if(tree) delete tree;

	return 0;
}
