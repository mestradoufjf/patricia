#define MAX_HEAP 0
#define MIN_HEAP 1

class THeap{
private:
	int heap_size;
	int max_size;
	int heap_mode;
	float *X;

public:
	THeap(int max_size, int heap_mode);
	~THeap();

	bool empty();
	bool full();
	int parent(int index);
	int left(int index);
	int right(int index);

	void heapfy(int index);
	float extract();

	void vectorToHeap(float* vector, int vector_size);

	void print();

	void mappedHeapfy(int index, int* map);
	float mappedExtract(int* map, int* map_index);
	void mappedVectorToHeap(float* vector, int vector_size, int* map);
};

float* heapSort(float* vector, int vector_size, int heap_mode);
float* mappedHeapSort(float* vector, int vector_size, int* map, int heap_mode);
