#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define DEBUG 1
#define TEST 0
#define LEAFSKIP -1

class RadixNode{
private:
	int skip;
	bool is_word;
	unsigned int alphabet_size, document_count;
	unsigned int *frequency;
	char *preffix;
	RadixNode **next;

public:
	RadixNode(unsigned int alphabet_size, unsigned int document_count);
	~RadixNode();

	int getSkip();
	bool isWord();
	unsigned int getAlphabetSize();
	unsigned int getDocumentCount();
	unsigned int getFrequency(unsigned int document);
	char* getPreffix();
	RadixNode* getNext(char character);
	RadixNode* getNext(unsigned int character);

	void setSkip(unsigned int skip);
	void setIsWord(bool is_word);
	void setFrequency(unsigned int frequency, unsigned int document);
	void setPreffix(char* preffix);
	void setNext(RadixNode* next, char character);

	void incFrequency(unsigned int document);
	int countDocumentHits();
};

class RadixTree{
private:
	char** document_names;
	unsigned int alphabet_size;
	unsigned int document_count;
	int *max_frequency, *distinct_words;
	RadixNode *root;

public:
	RadixTree(unsigned int alphabet_size, unsigned int document_count, char** document_names);
	~RadixTree();

	unsigned int getAlphabetSize();
	unsigned int getDocumentCount();
	char* getDocumentName(unsigned int document);
	int getDistinctWords(unsigned int document);
	int getMaxFrequency(unsigned int document);

	void countDistinctWords();
	void maxFrequency();

	RadixNode* search(char* word);
	void insert(char* word, unsigned int document);

	void printToFile(FILE* output_file);
	void alphaOrderVisit(RadixNode *root, FILE* output_file);

	int countDistinctWords(unsigned int document);
	int countDistinctWordsVisit(RadixNode *root, unsigned int document);
	int countDocumentHits(char* word);
	int maxFrequency(unsigned int document);
	int maxFrequencyVisit(RadixNode* root, unsigned int document);
};
