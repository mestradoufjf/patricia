#include <stdlib.h>
#include <iostream>
#include "THeap.h"

using namespace std;

THeap::THeap(int max_size, int heap_mode = MAX_HEAP){
	this->max_size = max_size;
	this->heap_size = 0;
	this->X = new float[max_size];
	this->heap_mode = heap_mode;
}

THeap::~THeap(){
	if(this->X)
		delete[] X;
}

bool THeap::empty(){
	return (this->heap_size == 0);
}

bool THeap::full(){
	return (this->heap_size == this->max_size);
}

int THeap::parent(int index){
	return (index-1)/2;
}

int THeap::left(int index){
	return (2*index)+1;
}

int THeap::right(int index){
	return (2*index)+2;
}

void THeap::heapfy(int index){
	int left, right, largest, smallest;
	float aux;

	left = (2*index)+1;
	right = (2*index)+2;
	if(this->heap_mode == MAX_HEAP){
		largest = index;

		if(left < this->heap_size && this->X[left] > this->X[largest])
			largest = left;
		if(right < this->heap_size && this->X[right] > this->X[largest])
			largest = right;
		if(largest != index){
			aux = this->X[index];
			this->X[index] = this->X[largest];
			this->X[largest] = aux;
			heapfy(largest);
		}
	}
	else{
		smallest = index;

		if(left < this->heap_size && this->X[left] < this->X[smallest])
			smallest = left;
		if(right < this->heap_size && this->X[right] < this->X[smallest])
			smallest = right;
		if(smallest != index){
			aux = this->X[index];
			this->X[index] = this->X[smallest];
			this->X[smallest] = aux;
			heapfy(smallest);
		}
	}
}

float THeap::extract(){
	float root;
	if(this->empty()){
		cout << "Error: Heap empty!" << endl;
		exit(1);
	}
	else{
		root = this->X[0];
		this->heap_size--;
		this->X[0] = this->X[this->heap_size];
		this->heapfy(0);

		return root;
	}
}

void THeap::vectorToHeap(float* vector, int vector_size){
	int i;

	if(vector_size > this->max_size){
		cout << "Error: Vector bigger than heap capacity!" << endl;
		exit(2);
	}

	if(!vector){
		cout << "Error: Null vector to heapfy!" << endl;
		exit(3);
	}

	this->heap_size = vector_size;
	for(i = 0; i < vector_size; i++)
		this->X[i] = vector[i];

	for(i = parent(this->heap_size-1); i >= 0; i--)
		heapfy(i);
}

void THeap::print(){
	int i;
	cout << "Heap = [";
	for(i = 0; i < this->heap_size - 1; i++){
		cout << this->X[i] << ", ";
	}
	cout << this->X[this->heap_size - 1] << "]" << endl;
}

void THeap::mappedHeapfy(int index, int* map){
	int left, right, largest, smallest, aux_i;
	float aux;

	if(!map){
		cout << "Error: Null map vector!" << endl;
		exit(2);
	}

	left = (2*index)+1;
	right = (2*index)+2;
	if(this->heap_mode == MAX_HEAP){
		largest = index;

		if(left < this->heap_size && this->X[left] > this->X[largest])
			largest = left;
		if(right < this->heap_size && this->X[right] > this->X[largest])
			largest = right;
		if(largest != index){
			aux = this->X[index];
			this->X[index] = this->X[largest];
			this->X[largest] = aux;

			aux_i = map[index];
			map[index] = map[largest];
			map[largest] = aux_i;

			mappedHeapfy(largest, map);
		}
	}
	else{
		smallest = index;

		if(left < this->heap_size && this->X[left] < this->X[smallest])
			smallest = left;
		if(right < this->heap_size && this->X[right] < this->X[smallest])
			smallest = right;
		if(smallest != index){
			aux = this->X[index];
			this->X[index] = this->X[smallest];
			this->X[smallest] = aux;

			aux_i = map[index];
			map[index] = map[smallest];
			map[smallest] = aux_i;

			mappedHeapfy(smallest, map);
		}
	}
}

float THeap::mappedExtract(int* map, int* map_index){
	float root;
	int _map_index;

	if(this->empty()){
		cout << "Error: Heap empty!" << endl;
		exit(1);
	}

	if(!map){
		cout << "Error: Null map vector!" << endl;
		exit(2);
	}

	if(!map_index){
		cout << "Error: Null map_index pointer!" << endl;
		exit(3);
	}

	this->heap_size--;

	root = this->X[0];
	this->X[0] = this->X[this->heap_size];

	_map_index = map[0];
	map[0] = map[this->heap_size];

	this->mappedHeapfy(0, map);

	*map_index = _map_index;
	return root;

}

void THeap::mappedVectorToHeap(float* vector, int vector_size, int* map){
	int i;

	if(vector_size > this->max_size){
		cout << "Error: Vector bigger than heap capacity!" << endl;
		exit(4);
	}

	if(!vector){
		cout << "Error: Null vector to heapfy!" << endl;
		exit(5);
	}

	if(!map){
		cout << "Error: Null map vector!" << endl;
		exit(2);
	}

	this->heap_size = vector_size;
	for(i = 0; i < vector_size; i++){
		this->X[i] = vector[i];
		map[i] = i;
	}

	for(i = parent(this->heap_size-1); i >= 0; i--){
		mappedHeapfy(i, map);
	}

}

float* heapSort(float* vector, int vector_size, int heap_mode){
	int i;
	float *sorted_vector = NULL;
	THeap *heap = NULL;

	if(!vector){
		cout << "Error: Null vector to sort!" << endl;
		exit(5);
	}

	sorted_vector = (float*) calloc(vector_size, sizeof(float));
	heap = new THeap(vector_size, heap_mode);

	if(heap){
		heap->vectorToHeap(vector, vector_size);

		for(i = 0; i < vector_size; i++){
			sorted_vector[i] = heap->extract();
		}
	}

	delete heap;
	return sorted_vector;
}

float* mappedHeapSort(float* vector, int vector_size, int* map, int heap_mode){
	int i;
	float *sorted_vector = NULL;
	THeap *heap = NULL;

	if(!vector){
		cout << "Error: Null vector to sort!" << endl;
		exit(5);
	}

	if(!map){
		cout << "Error: Null map vector!" << endl;
		exit(2);
	}

	sorted_vector = (float*) calloc(vector_size, sizeof(float));
	int aux_map[vector_size];
	heap = new THeap(vector_size, heap_mode);

	if(heap){
	heap->mappedVectorToHeap(vector, vector_size, map);

		for(i = 0; i < vector_size; i++){
			sorted_vector[i] = heap->mappedExtract(map, &aux_map[i]);
		}

		for(i = 0; i < vector_size; i++){
			map[i] = aux_map[i];
		}
	}

	delete heap;
	return sorted_vector;
}
