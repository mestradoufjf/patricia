#include <math.h>
#include <string.h>
#include <time.h>
#include <float.h>
#include <iostream>
#include "InvertedIndex.h"
#include "THeap.h"

using namespace std;

char** getFileNames(FILE* input_file, unsigned int* file_count){
	size_t _file_count, char_count = 0, doc_id, i;
	char buffer;
	char line[512];
	char** file_names;

	if(input_file){
		fscanf(input_file, "%lu", &_file_count);

		do{
			buffer = fgetc(input_file);
		}while(buffer != '\n');

		file_names = (char**) calloc(_file_count, sizeof(char*));

		for(doc_id = 0; doc_id < _file_count; doc_id++){
			if(fgets(line, 512, input_file)){
				char_count = 0;
				buffer = line[0];
				while(buffer != '\n' && buffer != '\r'){
					char_count++;
					buffer = line[char_count];
				}
				if(char_count){
					file_names[doc_id] = (char*) calloc(char_count+1, sizeof(char));
					for(i = 0; i < char_count; i++){
						file_names[doc_id][i] = line[i];
					}
					file_names[doc_id][char_count] = '\0';
				}
			}
		}

		if(file_count)
			*file_count = _file_count;
		return file_names;
	}
	return NULL;
}

char* getWord(FILE* input_file, unsigned int* char_count){
	unsigned int _char_count = 0, current_size = WORD_SIZE, i;
	char current_char;
	char *buffer, *aux_buffer, *word;

	if(input_file){
		buffer = (char*) calloc(WORD_SIZE, sizeof(char));
		if(!buffer) return NULL;

		current_char = fgetc(input_file);

		while(isspace(current_char) || ispunct(current_char) || isdigit(current_char) || iscntrl(current_char))
			current_char = fgetc(input_file);

		if(isalpha(current_char)){
			buffer[_char_count] = tolower(current_char);
			_char_count++;
			current_char = fgetc(input_file);

			while(isalnum(current_char)){
				buffer[_char_count] = tolower(current_char);
				_char_count++;

				if(_char_count == current_size){
					current_size *= 2;
					aux_buffer = (char*) realloc(buffer, sizeof(char)*current_size);
					if(!aux_buffer){
						free(buffer);
						return NULL;
					}
					buffer = aux_buffer;
					aux_buffer = NULL;
				}
				current_char = fgetc(input_file);
			}
		}
		if(_char_count){
			word = (char*) calloc(_char_count+1, sizeof(char));
			for(i = 0; i < _char_count; i++){
				word[i] = buffer[i];
			}
			word[_char_count] = '\0';
		}
		else word = NULL;

		free(buffer);

		*char_count = _char_count;
		return word;
	}
	return NULL;
}

RadixTree* createInvertedIndex(char** document_names, unsigned int document_count){
	unsigned int word_size, i, word_count = 0;
	char* word;
	time_t start, end;
	double time, min_time = FLT_MAX, max_time = FLT_MIN, avg_time = 0.0;
	FILE* input_file = NULL;
	RadixTree *tree = NULL;

	if(document_names){
		tree = new RadixTree(36, document_count, document_names);
		for(i = 0; i < document_count; i++){
			if(DEBUG)
				fprintf(stdout, "File[%d]: %s\n", i, document_names[i]);
			input_file = fopen(document_names[i], "r");
			if(input_file){
				word = getWord(input_file, &word_size);
				while(word){
					start = clock();
					tree->insert(word, i);
					end = clock();

					time = (double)(end - start) / CLOCKS_PER_SEC;
					if(time < min_time) min_time = time;
					if(time > max_time) max_time = time;
					avg_time += time;
					word_count++;

					word = getWord(input_file, &word_size);
				}
				fclose(input_file);
			}
			else{
				fprintf(stderr, "Fatal error: Failed to open file '%s'", document_names[i]);
				exit(2);
			}
		}
		avg_time = avg_time/(double)(word_count);
		if(TEST){
			printf("Insertion time:\n");
			printf("min: %6.10lf max: %6.10lf avg: %6.10lf\n", min_time, max_time, avg_time);
		}
		tree->countDistinctWords();
		tree->maxFrequency();
		return tree;
	}
	return NULL;
}

char* getSearchWord(char* search, unsigned int* pos, unsigned int* char_count){
	unsigned int _char_count = 0, current_size = WORD_SIZE, p = *pos, i;
	char current_char;
	char *buffer, *aux_buffer, *word;

	buffer = (char*) calloc(WORD_SIZE, sizeof(char));
	if(!buffer) return NULL;

	if(p >= strlen(search)) return NULL;

	current_char = search[p];
	p++;

	while(isspace(current_char) || ispunct(current_char) || isdigit(current_char) || iscntrl(current_char)){
		current_char = search[p];
		p++;
	}

	if(isalpha(current_char)){
		buffer[_char_count] = tolower(current_char);
		_char_count++;
		current_char = search[p];
		p++;

		while(isalnum(current_char)){
			buffer[_char_count] = tolower(current_char);
			_char_count++;

			if(_char_count == current_size){
				current_size *= 2;
				aux_buffer = (char*) realloc(buffer, sizeof(char)*current_size);
				if(!aux_buffer){
					free(buffer);
					return NULL;
				}
				buffer = aux_buffer;
				aux_buffer = NULL;
			}
			current_char = search[p];
			p++;
		}
	}

	if(_char_count){
		word = (char*) calloc(_char_count+1, sizeof(char));
		for(i = 0; i < _char_count; i++){
			word[i] = buffer[i];
		}
		word[_char_count] = '\0';
	}
	else word = NULL;

	*char_count = _char_count;
	*pos = p;

	free(buffer);
	return word;
}

void searchDocumentRelevance(char* search, RadixTree* tree, unsigned int document_count, float min_relevance){
	char 	*word = NULL,
			**buffer = NULL,
			**search_words = NULL,
			**aux_buffer = NULL;

	unsigned int	pos = 0,
					char_count = 0,
					word_count = 0,
					current_size = SEARCH_SIZE,
					i, j;

	if(!search){
		cout << "Error: Empty search!" << endl;
		exit(3);
	}

	if(!tree){
		cout << "Error: Null tree pointer!" << endl;
		exit(4);
	}

	/// Organizacao da busca em um vetor de palavras de tamanho word_count
	buffer = (char**) calloc(SEARCH_SIZE, sizeof(char*));
	if(!buffer){
		fprintf(stderr, "Fatal error: Failed to allocate memory (%lu bytes)", SEARCH_SIZE * sizeof(char*));
		exit(5);
	}

	word = getSearchWord(search, &pos, &char_count);
	while(word){
		if(word_count == current_size){
			current_size *= 2;
			aux_buffer = (char**) realloc(buffer, sizeof(char) * current_size);
			if(!aux_buffer){
				free(buffer);
				fprintf(stderr, "Fatal error: Failed to reallocate memory (%lu bytes)", sizeof(char) * current_size);
				exit(6);
			}
			buffer = aux_buffer;
			aux_buffer = NULL;
		}

		buffer[word_count] = word;
		word_count++;
		word = getSearchWord(search, &pos, &char_count);
	}

	if(word_count){
		search_words = (char**) calloc(word_count, sizeof(char*));
		for(i = 0; i < word_count; i++){
			search_words[i] = buffer[i];
		}
	}

	free(buffer);

	/// Processamento da relevancia
	float relevance[document_count];
	float 	weight = 0, weight_sum = 0,
			freq, max_freq, tf, idf, document_hits, distinct_words;

	RadixNode *node = NULL;

	for(j = 0; j < document_count; j++){
		weight_sum = 0;
		for(i = 0; i < word_count; i++){
			weight = 0;
			node = tree->search(search_words[i]);
			if(node){
				document_hits = (float) node->countDocumentHits();
				freq = (float) node->getFrequency(j);
				max_freq = (float) tree->getMaxFrequency(j);
				tf = freq / max_freq;
				idf = log(document_count / (1+document_hits));
				weight = tf * idf;
			}
			weight_sum += weight;
		}
		distinct_words = (float) tree->getDistinctWords(j);
		if(distinct_words)
			relevance[j] = weight_sum/distinct_words;
		else
			relevance[j] = 0.0;
	}

	/// Ordenacao dos documentos, em ordem decrescente de relevancia
	int map[document_count];
	float *ordered_relevance;

	ordered_relevance = mappedHeapSort(relevance, document_count, map, MAX_HEAP);

	/// Normalizacao das relevancias para o intervalo [0,1] onde necessariamente, a maior relevancia vale 1
	float norm_relevance[document_count];

	if(ordered_relevance[0] < PRECISION){
		for(i = 0; i < document_count; i++){
			norm_relevance[i] = 0.0;
		}
	}
	else{
		for(i = 0; i < document_count; i++){
			norm_relevance[i] = ordered_relevance[i]/ordered_relevance[0];
		}
	}


	/// Impressao das relevancias acima do parametro L
	if(!TEST){
		if(DEBUG){
			printf("Document relevance:\n");
			for(i = 0; i < document_count; i++){
				printf("Document '%s' relevance: %f \n", tree->getDocumentName(i), relevance[i]);
			}
			printf("\n");

			printf("Normalized relevance:\n");
			for(i = 0; i < document_count; i++){
				printf("Document '%s' relevance: %f \n", tree->getDocumentName(i), norm_relevance[i]);
			}
			printf("\n");

			printf("Ordered relevance:\n");
			for(i = 0; i < document_count; i++){
				printf("Document '%s' relevance: %f (%.3f)\n", tree->getDocumentName(map[i]), ordered_relevance[i], norm_relevance[i]);
			}
			printf("\n");
		}
		else{
			printf("Documents found:\n");
			if(min_relevance < PRECISION){
				for(i = 0; i < document_count; i++){
					printf("Document '%s' relevance: %f (%.3f)\n", tree->getDocumentName(map[i]), ordered_relevance[i], norm_relevance[i]);
				}
				if(i == 0) printf("(none)\n");
			}
			else{
				for(i = 0; i < document_count && norm_relevance[i] - min_relevance > PRECISION; i++){
					printf("Document '%s' relevance: %f (%.3f)\n", tree->getDocumentName(map[i]), ordered_relevance[i], norm_relevance[i]);
				}
				if(i == 0) printf("(none)\n");
			}
		}
	}

	if(ordered_relevance)
		free(ordered_relevance);
	for(i = 0; i < word_count; i++){
		free(search_words[i]);
	}
	free(search_words);
}
