#include "TRadixTree.h"

#define WORD_SIZE 128
#define SEARCH_SIZE 16
#define PRECISION 0.00000001

char** getFileNames(FILE* input_file, unsigned int* file_count);
void toLowerCase(char* str);
char* getWord(FILE* input_file, unsigned int* char_count);

RadixTree* createInvertedIndex(char** document_names, unsigned int document_count);

void searchDocumentRelevance(char* search, RadixTree* tree, unsigned int document_count, float min_relevance);
