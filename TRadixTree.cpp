#include "TRadixTree.h"

bool equals(char* str1, char* str2){
	char *p = str1, *q = str2;
	while(*p && *q){
		if(*p != *q) return false;
		p++; q++;
	}
	if(*p != *q) return false;
	else return true;
}

int indexChar(char character){
	if(isdigit(character))
		return character - '0';
	if(isalpha(character))
		return character - 'a' + '9' - '0' + 1;
	return -1;
}

char* substring(char* string, unsigned int size){
	char *substring = (char*) malloc(sizeof(char) * (size+1));

	for(unsigned int i = 0; i < size; i++){
		substring[i] = string[i];
	}
	substring[size] = '\0';
	return substring;
}

/** ***********************************************************************************************
*	**********************************	RADIX NODE	***********************************************
*	***********************************************************************************************
*/

RadixNode::RadixNode(unsigned int alphabet_size, unsigned int document_count){
	this->skip = LEAFSKIP;
	this->is_word = false;
	this->alphabet_size = alphabet_size;
	this->document_count = document_count;
	this->next = new RadixNode*[alphabet_size];
	for(unsigned int i = 0; i < alphabet_size; i++)
		this->next[i] = NULL;
	this->preffix = NULL;
	this->frequency = new unsigned int[document_count];
	for(unsigned int i = 0; i < document_count; i++)
		this->frequency[i] = 0;
}

RadixNode::~RadixNode(){
	delete[] this->frequency;
	if(this->preffix)
		free(this->preffix);
	for(unsigned int i = 0; i < alphabet_size; i++)
		delete this->next[i];
	delete[] this->next;
}

int RadixNode::getSkip(){
	return this->skip;
}

bool RadixNode::isWord(){
	return this->is_word;
}

unsigned int RadixNode::getAlphabetSize(){
	return this->alphabet_size;
}

unsigned int RadixNode::getDocumentCount(){
	return this->document_count;
}

unsigned int RadixNode::getFrequency(unsigned int document){
	if(document >= 0 && document < this->document_count)
		return this->frequency[document];
	else{
		fprintf(stderr, "Fatal error: RadixNode: Invalid document number. Document count = %d\n", this->document_count);
		exit(1);
	}
}

char* RadixNode::getPreffix(){
	return this->preffix;
}

RadixNode* RadixNode::getNext(char character){
	unsigned int i = indexChar(character);
	if(i >= 0 && i < this->alphabet_size)
		return this->next[i];
	else{
		fprintf(stderr, "Fatal error: RadixNode: Invalid character '%c'. Alphabet size = %d\n", character, this->alphabet_size);
		exit(2);
	}
}

RadixNode* RadixNode::getNext(unsigned int character){
	if(character >= 0 && character < this->alphabet_size)
		return this->next[character];
	else{
		fprintf(stderr, "Fatal error: RadixNode: Invalid character #%d. Alphabet size = %d\n", character, this->alphabet_size);
		exit(2);
	}
}

void RadixNode::setSkip(unsigned int skip){
	this->skip = skip;
}

void RadixNode::setIsWord(bool is_word){
	this->is_word = is_word;
}

void RadixNode::setFrequency(unsigned int frequency, unsigned int document){
	if(document >= 0 && document < this->document_count)
		this->frequency[document] = frequency;
	else{
		fprintf(stderr, "Fatal error: RadixNode: Invalid document number. Document count = %d\n", this->document_count);
		exit(3);
	}
}

void RadixNode::setPreffix(char* preffix){
	if(this->preffix) free(this->preffix);
	this->preffix = preffix;
}

void RadixNode::setNext(RadixNode* next, char character){
	unsigned int i = indexChar(character);
	if(i >= 0 && i < this->alphabet_size)
		this->next[i] = next;
	else{
		fprintf(stderr, "Fatal error: RadixNode: Invalid character '%c'. Alphabet size = %d\n", character, this->alphabet_size);
		exit(4);
	}
}

void RadixNode::incFrequency(unsigned int document){
	if(document >= 0 && document < this->document_count)
		this->frequency[document]++;
	else{
		fprintf(stderr, "Fatal error: RadixNode: Invalid document number. Document count = %d\n", this->document_count);
		exit(3);
	}
}

int RadixNode::countDocumentHits(){
	unsigned int i;
	int document_hits = 0;
	for(i = 0; i < this->document_count; i++){
		if(this->getFrequency(i) > 0)
			document_hits++;
	}
	return document_hits;
}

/** ***********************************************************************************************
*	**********************************	RADIX TREE	***********************************************
*	***********************************************************************************************
*/

RadixTree::RadixTree(unsigned int alphabet_size, unsigned int document_count, char** document_names){
	this->document_names = document_names;
	this->alphabet_size = alphabet_size;
	this->document_count = document_count;
	this->max_frequency = new int[this->document_count];
	this->distinct_words = new int[this->document_count];
	for(unsigned int i = 0; i < this->document_count; i++){
		this->max_frequency[i] = 0;
		this->distinct_words[i] = 0;
	}
	this->root = NULL;
}

RadixTree::~RadixTree(){
	if(this->document_names)
		for(unsigned int i = 0; i < this->document_count; i++)
			free(document_names[i]);
		free(document_names);
	if(this->root)
		delete this->root;
	delete[] this->max_frequency;
	delete[] this->distinct_words;
}

char* RadixTree::getDocumentName(unsigned int document){
	if(document >= 0 && document < this->document_count)
		return this->document_names[document];
	else{
		fprintf(stderr, "Fatal error: RadixTree: Invalid document number. Document count = %d\n", this->document_count);
		exit(3);
	}
}

int RadixTree::getDistinctWords(unsigned int document){
	if(document >= 0 && document < this->document_count)
		return this->distinct_words[document];
	else{
		fprintf(stderr, "Fatal error: RadixTree: Invalid document number. Document count = %d\n", this->document_count);
		exit(3);
	}
}

int RadixTree::getMaxFrequency(unsigned int document){
	if(document >= 0 && document < this->document_count)
		return this->max_frequency[document];
	else{
		fprintf(stderr, "Fatal error: RadixTree: Invalid document number. Document count = %d\n", this->document_count);
		exit(3);
	}
}

void RadixTree::countDistinctWords(){
	unsigned int i;
	if(this->root){
		for(i = 0; i < this->document_count; i++){
			this->distinct_words[i] = countDistinctWords(i);
		}
	}
}

void RadixTree::maxFrequency(){
	unsigned int i, j;
	int max_freq, freq;

	if(this->root){
		for(j = 0; j < this->document_count; j++){
			max_freq = 0;
			freq = 0;
			for(i = 0; i < this->alphabet_size; i++){
				freq = maxFrequencyVisit(this->root->getNext(i), j);
				if(freq > max_freq)
					max_freq = freq;
			}
			this->max_frequency[j] = max_freq;
		}
	}
}

RadixNode* RadixTree::search(char* word){
	RadixNode *current_node = NULL;
	int current_char, skips, word_length;

	if(word){
		word_length = strlen(word);
		if(this->root){

			current_char = 0;
			current_node = this->root->getNext(word[0]);

			if(current_node){
				skips = current_node->getSkip();

				while(skips < word_length && skips > 0){
					current_char = skips;
					if(current_node->getNext(word[current_char])){
						current_node = current_node->getNext(word[current_char]);
						skips = current_node->getSkip();
					}
					else
						return NULL;
				}
				if(skips == word_length || skips == LEAFSKIP){
					if(current_node->isWord())
						if(equals(word, current_node->getPreffix()))
							return current_node;
				}
				else
					return NULL;
			}
		}
	}
	return NULL;
}

void RadixTree::insert(char* word, unsigned int document){
	char *current_preffix, *preffix;
	int word_length, preffix_length, current_char;
	RadixNode *current_node, *parent_node, *new_node, *new_int_node;

	if(word){
		if(DEBUG)
			printf("Palavra a inserir: '%s'\t\t", word);

		if(!this->root){
			/// Arvore completamente vazia
			if(DEBUG)
				printf("Inseriu na arvore vazia.\n");

			new_node = new RadixNode(this->alphabet_size, this->document_count);
			new_node->setPreffix(word);
			new_node->setIsWord(true);
			new_node->setSkip(LEAFSKIP);
			new_node->incFrequency(document);

			this->root = new RadixNode(this->alphabet_size, this->document_count);
			this->root->setNext(new_node, word[0]);
			this->root->setSkip(0);

			return;
		}
		else{
			if(!this->root->getNext(word[0])){
				/// Subarvore da primeira letra vazia
				if(DEBUG)
					printf("Inseriu em subarvore vazia.\n");

				new_node = new RadixNode(this->alphabet_size, this->document_count);
				new_node->setPreffix(word);
				new_node->setIsWord(true);
				new_node->setSkip(LEAFSKIP);
				new_node->incFrequency(document);

				this->root->setNext(new_node, word[0]);

				return;
			}
			else{
				/// Casos de percorrimento da arvore
				parent_node = this->root;
				current_node = this->root->getNext(word[0]);
				current_preffix = current_node->getPreffix();

				preffix_length = strlen(current_preffix);
				word_length = strlen(word);

				current_char = 0;

				do{
                    if(equals(word, current_preffix)){
						/// Ja existe prefixo e ele se torna palavra, ou ja existe a palavra
						if(!current_node->isWord()){
							current_node->setIsWord(true);
							if(DEBUG)
								printf("Ja existe o prefixo.\n");
						}
						else{
							if(DEBUG)
								printf("Ja existe a palavra. Freq = %d\n", current_node->getFrequency(document)+1);
						}
						current_node->incFrequency(document);

						return;
					}

					/// Comparacao caracter a caracter
					for(current_char = 0; current_char < word_length; current_char++){
						if(current_char == preffix_length){
							parent_node = current_node;
							current_node = current_node->getNext(word[current_char]);
							if(current_node){
								current_preffix = current_node->getPreffix();
								preffix_length = strlen(current_preffix);
							}
						}
						else{
							if(current_node && current_preffix[current_char] != word[current_char]){
								/// Palavras partilham um mesmo prefixo
								preffix = substring(word, current_char);
								if(DEBUG)
									printf("Inseriu gerando prefixo '%s'\n", preffix);
								new_int_node = new RadixNode(this->alphabet_size, this->document_count);
								new_int_node->setPreffix(preffix);
								new_int_node->setSkip(current_char);

								new_node = new RadixNode(this->alphabet_size, this->document_count);
								new_node->setPreffix(word);
								new_node->setIsWord(true);
								new_node->setSkip(LEAFSKIP);
								new_node->incFrequency(document);

								parent_node->setNext(new_int_node, word[parent_node->getSkip()]);
								new_int_node->setNext(new_node, word[current_char]);
								new_int_node->setNext(current_node, current_node->getPreffix()[current_char]);

								return;
							}
						}
					}
					if(current_node && preffix_length > word_length){
						/// Palavra e prefixo de outra ja existente na arvore
						if(DEBUG)
							printf("Inseriu prefixo '%s'\n", word);
						new_int_node = new RadixNode(this->alphabet_size, this->document_count);
						new_int_node->setPreffix(word);
						new_int_node->setIsWord(true);
						new_int_node->setSkip(word_length);
						new_int_node->incFrequency(document);

						parent_node->setNext(new_int_node, word[parent_node->getSkip()]);
						new_int_node->setNext(current_node, current_preffix[word_length]);

						return;
					}
					if(!current_node && word_length > preffix_length){
						/// Insercao em folha, ja existe prefixo da palavra inserida
						if(DEBUG)
							printf("Inseriu em folha.\n");
						new_node = new RadixNode(this->alphabet_size, this->document_count);
						new_node->setPreffix(word);
						new_node->setIsWord(true);
						new_node->setSkip(LEAFSKIP);
						new_node->incFrequency(document);

						parent_node->setSkip(preffix_length);
						parent_node->setNext(new_node, word[preffix_length]);

						return;
					}
				}while(current_node);
				free(word);
			}
		}
	}
}

void RadixTree::printToFile(FILE* output_file){
	if(output_file && this->root){
		alphaOrderVisit(this->root, output_file);
	}
}

void RadixTree::alphaOrderVisit(RadixNode *root, FILE* output_file){
	unsigned int i, document_count;

	if(root){
		if(root->isWord()){
			if(!TEST){
				fprintf(output_file, "%-25s\t", root->getPreffix());
				document_count = root->getDocumentCount();
				for(i = 0; i < document_count; i++){
					fprintf(output_file, "%s: %d\t", document_names[i], root->getFrequency(i));
				}
				fprintf(output_file, "\n");
			}
		}

		for(i = 0; i < this->alphabet_size; i++){
			alphaOrderVisit(root->getNext(i), output_file);
		}
	}
}

int RadixTree::countDistinctWordsVisit(RadixNode *root, unsigned int document){
	unsigned int i;
	int distinct_words = 0;

	if(root){
		for(i = 0; i < this->alphabet_size; i++){
			distinct_words += countDistinctWordsVisit(root->getNext(i), document);
		}

		if(root->isWord()){
			if(root->getFrequency(document)){
				distinct_words++;
			}
		}
	}
	return distinct_words;
}

int RadixTree::countDistinctWords(unsigned int document){
	int distinct_words = 0;
	if(this->root){
		distinct_words = countDistinctWordsVisit(this->root, document);
	}
	return distinct_words;
}

int RadixTree::countDocumentHits(char* word){
	RadixNode* node = NULL;
	unsigned int i;
	int document_hits = 0;

	node = search(word);
	if(node){
		for(i = 0; i < this->document_count; i++){
			if(node->getFrequency(i) != 0)
				document_hits++;
		}
	}
	return document_hits;
}

int RadixTree::maxFrequency(unsigned int document){
	unsigned int i;
	int max_freq = 0, freq = 0;

	if(this->root){
		for(i = 0; i < this->alphabet_size; i++){
			freq = maxFrequencyVisit(this->root->getNext(i), document);
			if(freq > max_freq)
				max_freq = freq;
		}
	}
	return max_freq;
}

int RadixTree::maxFrequencyVisit(RadixNode* root, unsigned int document){
	unsigned int i;
	int max_freq = 0, freq = 0;

	if(root){
		max_freq = root->getFrequency(document);
		for(i = 0; i < this->alphabet_size; i++){
			freq = maxFrequencyVisit(root->getNext(i), document);
			if(freq > max_freq)
				max_freq = freq;
		}
	}
	return max_freq;
}
